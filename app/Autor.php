<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $fillable = [
        'autor','autor_id'
    ];

    protected $table = 'autors';

    protected $primaryKey = 'autor_id';

    public function autori(){
        return $this->hasMany('App\Knjige','autor_id');
    }
}
