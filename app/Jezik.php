<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jezik extends Model
{
    protected $fillable = [
        'jezik','jezik_id'
    ];

    protected $table = 'jeziks';

    protected $primaryKey = 'jezik_id';

    public function jezici(){
        return $this->hasMany('App\Knjige','jezik_id');
    }

    public function originalni_jezici(){
        return $this->hasMany('App\Knjige','originalni_jezik_id');
    }

    public function ukupno(){
        return $this->jezici->merge($this->originalni_jezici());
    }
}
