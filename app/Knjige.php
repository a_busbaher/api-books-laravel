<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Knjige extends Model
{
    protected $fillable = [
        'id', 'naslov'
        ,'autor_id','jezik_id',
        'originalni_jezik_id',
        'godina_izdavanja',
        'created_at','updated_at'
    ];

    protected $table = 'knjiges';

    public function jezik()
    {
        return $this->belongsTo('App\Jezik', 'jezik_id');
    }

    public function originalni_jezik()
    {
        return $this->belongsTo('App\Jezik', 'originalni_jezik_id');
    }

    public function ukupno_j(){
        return $this->jezik->merge($this->originalni_jezik());
    }

    public function autor()
    {
        return $this->belongsTo('App\Autor', 'autor_id');
    }
}
