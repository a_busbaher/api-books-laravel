<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Knjige\Knjizara;
use Validator;

class BooksController extends Controller
{
    protected $books;
    

    /**
     * BooksController constructor.
     * @param Knjizara $knjige
     * DEPENDENCY INJECTION KLASE KNJIŽARA
     */
    public function  __construct(Knjizara $knjige){
        $this->books = $knjige;
        //ZAŠTITA RUTA ZA KREIRANJE,BRISANJE I AŽURIRANJE KNJIGE
        $this->middleware('jwt.auth', ['only' => ['create', 'update', 'destroy']]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * DOBAVLJANJE SVIH KNJIGA(10 po strani)
     * URL: ..../api/books?page=1
     */
    public function index(){
        $data = $this->books->getBooks();
        return response()->json($data);
    }



    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * DOBAVLJANJE JEDNE KNJIGA
     * URL: .../api/book/1
     */
    public function show($id){
        $data = $this->books->getOneBook($id);
        if(!$data){
            return response()->json(['message' => 'Tražena knjiga ne postoji'],404);
        }
        return response()->json($data);

    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * PRETRAGA KNJIGA PO GODINI IZDAVANJA ILI AUTORU
     * URL: .../api/books/search?params=1942
     */
    public function search(Request $request){
        $parameters = request()->get('params');
        $data = $this->books->searchBooks($parameters);
        if(!$data){
            return response()->json(['message' => 'Nema rezultata pretrage'],200);
        }
        return response()->json($data);
    }

    /**
     * @param CreateBookRequest $request
     * @return \Illuminate\Http\JsonResponse
     * DODAVANJE NOVE KNJIGE
     * URL: .../api/book/create
     */
    public function create(CreateBookRequest $request){
        try{
            $data = $this->books->createBook($request);
            return response()->json($data,201);
        }catch (Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }



    /**
     * @param UpdateBookRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * AŽIRARANJE POSTOJEĆE KNJIGE
     * URL: .../api/book/update/1
     */
    public function update(UpdateBookRequest $request, $id){
        try{
            $data = $this->books->updateBook($request, $id);
            return response()->json($data,200);
        }catch (ModelNotFoundException $ex){
            throw $ex;
        }catch (Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * BRISANJE KNJIGE
     * URL: .../api/book/delete/1
     */
    public function destroy($id){
        try{
            $data = $this->books->deleteBook($id);
            return response()->json(['message' => 'Knjiga uspešno obrisana'],200);
        }catch (ModelNotFoundException $ex){
            throw $ex;
        }catch (Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }
    }
}
