<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use JWTAuthException;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    private $user;

    /**
     * AuthController constructor.
     * @param User $user
     * @param JWTAuth $jwtauth
     * PACKIGIST JWTAUTH ZA DODELJIVANJE  TOKENA
     */
    public function __construct(User $user, JWTAuth $jwtauth)
    {
        $this->user = $user;
        $this->jwtauth = $jwtauth;
    }



    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     * REGISTRACIJA NOVIH KORISNIKA
     * URL: .../auth/register
     */
    public function register(RegisterRequest $request){
    $newUser = $this->user->create([
        'name' => $request->get('name'),
        'email' => $request->get('email'),
        'password' => bcrypt($request->get('password'))
    ]);
    if (!$newUser) {
        return response()->json(['failed_to_create_new_user'], 500);
    }
    return response()->json([
        'message' => 'Uspešna registracija,ulogujte se za token'
        ],200);
    }



    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * LOGOVANJE KORISNIKA
     * URL: .../auth/login
     */
    public function login(LoginRequest $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
            $token = $this->jwtauth->attempt($credentials);
            if (!$token) {
                return response()->json(['invalid_email_or_password'], 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
}
