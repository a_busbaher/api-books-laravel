<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'namespace' => 'Api'
], function () {
    //REGISTRACIJA NOVIH KORISNIKA
    Route::post('/auth/register', [
        'as' => 'auth.register',
        'uses' => 'AuthController@register'
    ]);
    //LOGOVANJE KORISNIKA
    Route::post('/auth/login', [
        'as' => 'auth.login',
        'uses' => 'AuthController@login'
    ]);
});
//  LISTANJE SVIH KNJIGE (10 KNJIGA PO STRANI)
Route::get('/api/books',['uses'=>'BooksController@index','as'=>'books']);
//  DOBAVLJANJE JEDNE KNJIGE
Route::get('/api/book/{id}',['uses'=>'BooksController@show','as'=>'book']);
//  PRETRAGA KNJIGA PO AUTORU ILI GODINI IZDAVANJA
Route::get('/api/books/search',['uses'=>'BooksController@search','as'=>'books.search']);


/*  ZAŠTIĆENJE RUTE
    DODAVANJE NOVE KNJIGE U  BAZU      */
Route::post('/api/book/create', ['uses'=>'BooksController@create','as'=>'book.create']);
//  AŽURIRANJE POSTOJEĆE KNJIGE
Route::put('/api/book/update/{id}', ['uses'=>'BooksController@update','as'=>'book.edit']);
//  BRISANJE KNJIGE IZ BAZE
Route::delete('/api/book/delete/{id}', ['uses'=>'BooksController@destroy','as'=>'book.delete']);