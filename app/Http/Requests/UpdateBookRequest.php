<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateBookRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'naslov'              => 'required',
            'autor'               => 'required',
            'godina_izdavanja'    => 'required|numeric',
            'jezik'               => 'required',
            'originalni_jezik'    => 'required',
        ];
    }
}
