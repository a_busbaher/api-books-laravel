<?php
namespace App\Knjige;

use App\Knjige;
use App\Autor;
use App\Jezik;

class Knjizara {

    /**
     * @return array
     * ISPIS SVIH KNJIGA(10 PO STRANI)
     */
    public function getBooks(){
        return $this->ispis(Knjige::paginate(10));
    }

    /**
     * @param $id
     * @return array
     * DOBAVLJANJE JEDNE KNJIGE PO ID-u
     */
    public function getOneBook($id){
        return $this->ispis(Knjige::where('id',$id)->get());
    }


    /**
     * @param $parameters
     * @return array
     * PRETRAGA KNJIGA PO AUTORU ILI GODINI IZDAVANJA
     */
    public function searchBooks($parameters){
        if(!empty($parameters)) {
            return $this->ispis(Knjige::whereHas('autor',function($query)use($parameters){
                $query->where('autor', 'LIKE', '%' . $parameters . '%');
            })
                ->orWhere('godina_izdavanja', 'LIKE', '%' . $parameters . '%')
                ->limit(10)
                ->get());
        }
    }


    /**
     * @param $knjige
     * @return array
     * PRIPREMA ISPISA PODATAKA O KNJIZI/KNJIGAMA
     */
    protected function ispis($knjige){
        $data =[];
        foreach ($knjige as $knjiga){
            $entry = [
                "Id:"                => $knjiga->id,
                "Naslov:"            => $knjiga->naslov,
                "Autor:"             => $knjiga->autor->autor,
                "Godina izdavanja:"  => $knjiga->godina_izdavanja,
                "Jezik:"             => $knjiga->jezik->jezik,
                "Originalni jezik"   => $knjiga->originalni_jezik->jezik,
            ];
            $data[] = $entry;
        }
        return $data;
    }


    /**
     * @param $field
     * @param $req
     * @return Jezik
     * PROVERA DA LI JEZIK I ORIGINALNI JEZIK POSTOJE
     * U BAZI I NJIHOV UPIS AKO NE POSTOJI
     */
    protected function ifJezikExists($field,$req){
        $jezik = Jezik::where('jezik', '=', $req->input($field))->first();
           if ($jezik === null) {
               $jezik = new Jezik();
               $jezik->jezik = $req->input($field);
               $jezik->save();
           }
           $jezik->jezik = $req->input($field);
           return $jezik;
        }


    /**
     * @param $req
     * @return Autor
     * //PROVERA DA LI AUTOR POSTOJI U BAZI I NJEGOV UPIS AKO NE POSTOJI
     */
    protected function ifAutorExists($req){
        $autor = Autor::where('autor', '=', $req->input('autor'))->first();
        if ($autor === null) {
            $autor = new Autor();
            $autor->autor = $req->input('autor');
            $autor->save();
        }
        $autor->autor = $req->input('autor');
        return $autor;
    }


    /**
     * @param $req
     * @return array
     * UPIS NOVE KNJIGE U BAZU
     */
    public function createBook($req){
        //PROVERA DA LI JEZIK,ORIGINALNI JEZIK I AUTOR POSTOJI U BAZI I NJEGOV UPIS AKO NE POSTOJI
        $autor = $this->ifAutorExists($req);
        $jezik = $this->ifJezikExists('jezik',$req);
        $jezik2 = $this->ifJezikExists('originalni_jezik',$req);

        //UPIS KNJIGE U BAZU
        $knjiga = new Knjige();
        $knjiga->autor()->associate($autor);
        $knjiga->jezik()->associate($jezik);
        $knjiga->originalni_jezik()->associate($jezik2);
        $knjiga->naslov = $req->input('naslov');
        $knjiga->godina_izdavanja = $req->input('godina_izdavanja');
        $knjiga->save();

        return $this->ispis([$knjiga]);
    }

    /**
     * @param $req
     * @param $id
     * @return array
     * IZMENA KNJIGE U BAZI
     */
    public function updateBook($req,$id){
        $knjiga = Knjige::where('id',$id)->firstOrFail();
        //PROVERA DA LI JEZIK,ORIGINALNI JEZIK I AUTOR POSTOJI U BAZI I NJEGOV UPIS AKO NE POSTOJI
        $autor = $this->ifAutorExists($req);
        $jezik = $this->ifJezikExists('jezik',$req);
        $jezik2 = $this->ifJezikExists('originalni_jezik',$req);

        //IZMENA KNJIGE U BAZI
        $knjiga->id = $id;
        $knjiga->autor()->associate($autor);
        $knjiga->jezik()->associate($jezik);
        $knjiga->originalni_jezik()->associate($jezik2);
        $knjiga->naslov = $req->input('naslov');
        $knjiga->godina_izdavanja = $req->input('godina_izdavanja');
        $knjiga->save();

        return $this->ispis([$knjiga]);
    }


    /**
     * @param $id
     * BRISANJE KNJIGE IZ BAZE
     */
    public function deleteBook($id){
        $knjiga = Knjige::where('id',$id)->firstOrFail();
        $knjiga->delete();
    }


}