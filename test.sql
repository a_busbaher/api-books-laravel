-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2017 at 09:32 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `autors`
--

CREATE TABLE IF NOT EXISTS `autors` (
  `autor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `autor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`autor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `autors`
--

INSERT INTO `autors` (`autor_id`, `autor`, `created_at`, `updated_at`) VALUES
(1, 'Alber Kami', '2017-04-13 13:42:41', '2017-04-13 13:42:41'),
(2, 'Marsel Prust', '2017-04-13 13:42:42', '2017-04-13 13:42:42'),
(3, 'Franc Kafka', '2017-04-13 13:42:42', '2017-04-13 13:42:42'),
(4, 'Antoan de Sent Egziperi', '2017-04-13 13:42:42', '2017-04-13 13:42:42'),
(5, 'Ežen Jonesko', '2017-04-13 14:17:50', '2017-04-13 14:17:50'),
(6, 'Gijom Apoliner', '2017-04-13 14:20:09', '2017-04-13 14:20:09'),
(7, 'asdfasfdsdf', '2017-04-13 14:47:09', '2017-04-13 14:47:09'),
(8, 'Fjodor Ilič Dostojevski', '2017-04-13 17:27:05', '2017-04-13 17:27:05'),
(9, 'Branko Ćopić', '2017-04-13 17:28:18', '2017-04-13 17:28:18'),
(10, 'Dragoslav Mihailović', '2017-04-13 17:30:11', '2017-04-13 17:30:11'),
(11, 'Luis Kerol', '2017-04-13 17:31:06', '2017-04-13 17:31:06'),
(12, 'Ivo Andrić', '2017-04-13 17:33:44', '2017-04-13 17:33:44'),
(13, 'Oskar Vajld', '2017-04-14 15:12:18', '2017-04-14 15:12:18'),
(14, 'Oskar Vajiouo', '2017-04-14 15:24:03', '2017-04-14 15:24:03'),
(15, 'Oskar Vajiouosaf', '2017-04-14 15:28:59', '2017-04-14 15:28:59'),
(16, 'Tolkin', '2017-04-15 15:27:04', '2017-04-15 15:27:04'),
(17, 'Tolkinsdfas', '2017-04-15 15:31:30', '2017-04-15 15:31:30'),
(18, 'Serđo Boneli', '2017-04-15 15:41:38', '2017-04-15 15:41:38'),
(19, 'marko1', '2017-04-15 17:10:50', '2017-04-15 17:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `jeziks`
--

CREATE TABLE IF NOT EXISTS `jeziks` (
  `jezik_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jezik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jezik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `jeziks`
--

INSERT INTO `jeziks` (`jezik_id`, `jezik`, `created_at`, `updated_at`) VALUES
(1, 'srpski', '2017-04-13 13:42:41', '2017-04-13 13:42:41'),
(2, 'engleski', '2017-04-13 13:42:41', '2017-04-13 13:42:41'),
(3, 'francuski', '2017-04-13 13:42:41', '2017-04-13 13:42:41'),
(4, 'ruski', '2017-04-13 13:42:41', '2017-04-13 13:42:41'),
(17, 'nemački', '2017-04-15 15:41:38', '2017-04-15 15:41:38'),
(18, 'italijanski', '2017-04-15 15:41:39', '2017-04-15 15:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `knjiges`
--

CREATE TABLE IF NOT EXISTS `knjiges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `autor_id` int(10) unsigned NOT NULL,
  `jezik_id` int(10) unsigned NOT NULL,
  `originalni_jezik_id` int(10) unsigned NOT NULL,
  `godina_izdavanja` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `knjiges`
--

INSERT INTO `knjiges` (`id`, `naslov`, `autor_id`, `jezik_id`, `originalni_jezik_id`, `godina_izdavanja`, `created_at`, `updated_at`) VALUES
(1, 'Stranac', 1, 1, 3, 1942, '2017-04-13 13:42:42', '2017-04-13 13:42:42'),
(2, 'U traganju za izgubljenim vremenom', 2, 1, 2, 1913, '2017-04-13 13:42:42', '2017-04-13 13:42:42'),
(3, 'Proces', 3, 1, 2, 1925, '2017-04-13 13:42:42', '2017-04-13 13:42:42'),
(4, 'Ćelava pevačica', 5, 1, 3, 1949, '2017-04-13 14:17:51', '2017-04-13 14:17:51'),
(5, 'Alkohol', 6, 1, 3, 1972, '2017-04-13 14:20:10', '2017-04-13 14:20:10'),
(6, 'Alisa u zemlji čuda', 11, 1, 2, 1975, '2017-04-13 14:47:09', '2017-04-13 17:31:06'),
(7, 'Kad su cvetale tivke', 10, 1, 1, 1972, '2017-04-13 15:22:38', '2017-04-13 17:30:11'),
(9, 'Zločina i kazna', 8, 1, 4, 1952, '2017-04-13 17:27:05', '2017-04-13 17:27:05'),
(10, 'Orlovi rano lete', 9, 1, 1, 1963, '2017-04-13 17:28:18', '2017-04-13 17:28:18'),
(11, 'Kad su cvetale tikve', 10, 1, 1, 1974, '2017-04-13 17:32:15', '2017-04-13 17:32:15'),
(12, 'Na drini ćuprija', 12, 1, 1, 1968, '2017-04-13 17:33:44', '2017-04-13 17:33:44'),
(13, 'Travnička hronika', 12, 1, 1, 1971, '2017-04-13 17:36:15', '2017-04-15 15:40:07'),
(14, 'Zagor', 18, 17, 18, 1971, '2017-04-14 14:01:59', '2017-04-15 15:41:39'),
(17, 'Portret Doriana Greja', 13, 1, 2, 1975, '2017-04-14 15:32:03', '2017-04-14 15:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_04_13_151904_create_jeziks_table', 2),
('2017_04_13_151924_create_knjiges_table', 2),
('2017_04_13_151951_create_autors_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sale', 'sale@s.cm', '$2y$10$uy5s5W8zp/l8AXbDS8dq6usEQhwdZMB0GhFjmlAJxwZVARPAXUWNG', NULL, '2017-04-13 09:52:41', '2017-04-13 09:52:41'),
(2, 'sima', 'sima@s.com', '$2y$10$tzuCR/WCT0Pna1y4YSBFj.HBBcSlA4QVZLm09YEWXJ5yDTQvXKo/W', NULL, '2017-04-13 14:03:44', '2017-04-13 14:03:44'),
(3, 'zika', 'zika@z.com', '$2y$10$ftYE4jldsjCb3wDg5iRJ8u2xy.NSB41rxFQ7qZ/WcdrefHuMBKsoC', NULL, '2017-04-13 17:39:49', '2017-04-13 17:39:49'),
(4, 'pera', 'pera@p.com', '$2y$10$h47J7d.L/htUmRpJYmsP0uiTKYSG7L3Y3/A8k1TPW3arD2NNYsJva', NULL, '2017-04-13 18:35:44', '2017-04-13 18:35:44'),
(5, 'ana', 'ana@a.com', '$2y$10$jx.DFZV.g/21RyHaj4QzR.Z9wUSJuDPCNH1hBjNVpSsIukQ8Qxecy', NULL, '2017-04-14 21:33:27', '2017-04-14 21:33:27'),
(6, 'Radojka', 'radojka@rada.rs', '$2y$10$BVWehu3IO85frK64glJgsOTgbUkSrzVQ4tO6.SNStNimpVXpNxAMi', NULL, '2017-04-14 22:22:34', '2017-04-14 22:22:34'),
(8, 'Marko', 'marko@m.com', '$2y$10$RjyqtJmSm.7Uai.ynC09TuaJKRDiXwTvqmojnTAaNMJ9fdMQTg5Ti', NULL, '2017-04-15 17:06:54', '2017-04-15 17:06:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
