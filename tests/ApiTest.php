<?php

use App\Jezik;
use App\Autor;
use App\Knjige;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ApiTest extends TestCase {

    use DatabaseMigrations;

    /**
     * @return RANDOM UPIS U TABELE
     * jezik,autor i knjige
     */
    private function creatingBookData(){
        $jezik = factory(Jezik::class)->create();
        $autor = factory(Autor::class)->create();
        $knjiga = factory(Knjige::class)->create
            (['autor_id' => $autor->autor_id,
            'jezik_id' =>$jezik->jezik_id,
            'originalni_jezik_id' =>$jezik->jezik_id]);
        return $knjiga;
    }

    /**
     * @return DUMMY PODACI ZA
     * INPUT CREATE I UPDATE KNJIGE
     */
    private function dummyBookData(){
        return ['naslov' => 'Neki naslov', 'autor' => 'dsfa',
            'godina_izdavanja' => 1954,'jezik' => 'sfafa',
            'originalni_jezik' => 'sfaffsa'];
    }

    /**
     * TEST ZA DOBALJANJE ODREĐENJE KNJIGE PO ID-U
     */
    public function testGetBookByIdIsWorking(){
        $knjiga = $this->creatingBookData();
        $this->json('GET', '/api/book/'.$knjiga->id)
            ->assertResponseOk()
            ->seeJsonEquals([[
                    "Id:"=> $knjiga->id,
                    "Naslov:" => $knjiga->naslov,
                    "Autor:" => $knjiga->autor->autor,
                    "Godina izdavanja:" => $knjiga->godina_izdavanja,
                    "Jezik:" => $knjiga->jezik->jezik,
                    "Originalni jezik"=> $knjiga->originalni_jezik->jezik
                ]]
            );
    }

    /**
     * TEST ZA PRETRAGU KNJIGA PO
     * AUTORU ILI GODINI IZDAVANJA
     */
    public function testSearchByParamsIsWorking(){
        $knjiga = $this->creatingBookData();
        $this->json('GET', '/api/books/search?params='.$knjiga->autor->autor)
            ->assertResponseOk()
            ->seeJsonStructure([
                ['Id:','Naslov:','Godina izdavanja:','Autor:']
            ]);
    }

    /**
     * TEST ZA ERROR 404 KOD NEPOSTOJEĆEG URL-a
     */
    public function test404IsShowingWithBadRoute(){
        $this->json('GET', '/api/sadfklfsksf')
            ->assertResponseStatus(404);
    }

    /**
     * TEST DA LI BRISANJE KNJIGE NE PROLAZI KAD
     * NIJE PROSLEDJEN TOKEN
     */
    public function testIsDeleteMethodNotAllowingDeletingWithoutAToken(){
        $knjiga = $this->creatingBookData();
        $this->json('DELETE', '/api/book/delete/1')
            ->assertResponseStatus(400);
    }

    /**
     * TEST DA LI DODAVANJE NOVE KNJIGE NE PROLAZI KAD
     * NIJE PROSLEDJEN TOKEN
     */
    public function testCreatingBookNotPossibleWithoutAToken(){
        $knjiga = $this->creatingBookData();
        $this->json('post', '/api/book/create',$this->dummyBookData())
            ->assertResponseStatus(400);
    }

    /**
     * TEST DA LI AŽURIRANJE KNJIGE NE PROLAZI KAD
     * NIJE PROSLEDJEN TOKEN
     */
    public function testUpdateBookNotPossibleWithoutToken(){
        $knjiga = $this->creatingBookData();
        $this->json('PUT', '/api/book/update/1',$this->dummyBookData())
            ->assertResponseStatus(400);
    }

    /**
     * TEST ZA BRISANJE KNJIGE
     */
    public function testDeleteBookWorkingWithMiddlewareDisabled(){
        $knjiga = $this->creatingBookData();
        $this->withoutMiddleware();
        $this->json('DELETE', '/api/book/delete/1')
            ->noTseeInDatabase('knjiges',['id' => 1,
                'naslov' => $knjiga->naslov ])
            ->assertResponseOk();
    }

    /**
     * TEST ZA KREIRANJE NOVE KNJIGE
     */
    public function testCreateBookWorkingWithMiddlewareDisabled(){
        $knjiga = $this->creatingBookData();
        $this->withoutMiddleware();
        $this->json('POST', '/api/book/create', $this->dummyBookData())
            ->seeInDatabase('knjiges',['naslov' => 'Neki naslov',
                'godina_izdavanja' => 1954])
            ->assertResponseStatus(201);
    }

    /**
     * TEST ZA AŽURIRANJE POSTOJĆE KNJIGE
     */
    public function testUpdateBookWorkingWithMiddlewareDisabled(){
        $knjiga = $this->creatingBookData();
        $this->withoutMiddleware();
        $this->json('PUT', '/api/book/update/'.$knjiga->id, $this->dummyBookData())
            ->seeInDatabase('knjiges',['naslov' => 'Neki naslov',
            'godina_izdavanja' => 1954])
            ->assertResponseStatus(200);
    }

}
