<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Jezik::class, function (Faker\Generator $faker) {
    return [
        'jezik' => $faker->name
    ];
});

$factory->define(App\Autor::class, function (Faker\Generator $faker) {
    return [
        'autor' => $faker->name
    ];
});

$factory->define(App\Knjige::class, function (Faker\Generator $faker) {
    return [
        'naslov' => $faker->name,
        'godina_izdavanja' => str_random(4)
    ];
});