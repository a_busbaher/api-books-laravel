<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JezikTableSeader::class);
        $this->call(AutorTableSeader::class);
        $this->call(KnjigeTableSeader::class);
    }
}
