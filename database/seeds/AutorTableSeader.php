<?php

use Illuminate\Database\Seeder;

class AutorTableSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        $autor = new \App\Autor(['autor'=>'Alber Kami']);
        $autor->save();

        $autor = new \App\Autor(['autor'=>'Marsel Prust']);
        $autor->save();

        $autor = new \App\Autor(['autor'=>'Franc Kafka']);
        $autor->save();

        $autor = new \App\Autor(['autor'=>'Antoan de Sent Egziperi']);
        $autor->save();
    }
}
