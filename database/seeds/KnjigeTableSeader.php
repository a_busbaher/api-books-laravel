<?php

use Illuminate\Database\Seeder;

class KnjigeTableSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        $knjiga = new \App\Knjige(['autor_id' => 1,'naslov' => 'Stranac',
            'godina_izdavanja' => 1942,'jezik_id' => 1,
            'originalni_jezik_id' => 3]);
        $knjiga->save();

        $knjiga = new \App\Knjige(['autor_id' => 2,'naslov' => 'U traganju za izgubljenim vremenom',
            'godina_izdavanja' => 1913,'jezik_id' => 1,
            'originalni_jezik_id' => 2]);
        $knjiga->save();

        $knjiga = new \App\Knjige(['autor_id' => 3,'naslov' => 'Proces',
            'godina_izdavanja' => 1925,'jezik_id' => 1,
            'originalni_jezik_id' => 2]);
        $knjiga->save();

    }
}
