<?php

use Illuminate\Database\Seeder;

class JezikTableSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jezik = new \App\Jezik(['jezik'=>'srpski']);
        $jezik->save();

        $jezik = new \App\Jezik(['jezik'=>'engleski']);
        $jezik->save();

        $jezik = new \App\Jezik(['jezik'=>'francuski']);
        $jezik->save();

        $jezik = new \App\Jezik(['jezik'=>'ruski']);
        $jezik->save();

    }
}
