<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKnjigesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knjiges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naslov');
            $table->integer('autor_id')->unsigned();;
            $table->integer('jezik_id')->unsigned();;
            $table->integer('originalni_jezik_id')->unsigned();;
            $table->integer('godina_izdavanja');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('knjiges');
    }
}
